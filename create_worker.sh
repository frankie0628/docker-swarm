#! /bin/bash

TOKEN=$1
MANAGER_IP=$2

docker swarm join --token $TOKEN $MANAGER_IP:2377

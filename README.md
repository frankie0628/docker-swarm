# docker-swarm

Docker swam consists of serveral Docker hosts. You can have one host acts as manager and other hosts to be workers,
or mutilple managers to handle all the workers for high availability

## Instruction

1. create a manager
2. create serveral workers to join the cluster
3. create the services from the manager server and define how many replicas to distribute across all the workers

### Create a manager

`./create_manager.sh [the public ip address of the manager server]`

### Create a worker

**In the worker server**, run:

`./create_workder.sh <TOKEN> <IP-ADDRESS-OF-MANAGER>` or the `docker swarm join` cmd below.

**In the manager server**, run `docker swarm join-token worker`, it will display the cmd for a worker to join the cluster

`docker swarm join --token SWMTKN-1-4w90kys2d2n3nfmdmfe6xbrda63567ue9y4khib5x2cfhqllt6-2yokkjj5shko1v02sxrm4g4gl 104.200.24.133:2377`

TOKEN = SWMTKN-1-4w90kys2d2n3nfmdmfe6xbrda63567ue9y4khib5x2cfhqllt6-2yokkjj5shko1v02sxrm4g4gl

IP-ADDRESS-OF-MANAGER = 104.200.24.133

### Create a service and distribute to all the workers

**In the manager server** `docker service create --name <SERVICE-NAME> -p 80:80 --replicas <REPLICAS> <IMAGE-NAME>

SERVICE-NAME can be something like `api`
REPLICAS can be any numbers, the swarm manager will ditribute it evenly across all the workers

### Useful CMD

- **In the manager server**, `docker node ls` will list all the nodes(workers)
